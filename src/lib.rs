use std::borrow::Borrow;

pub trait Wrapper {
    type Type<T>: Borrow<T>;
}

pub struct Rc;
pub struct Arc;

impl Wrapper for () {
    type Type<T> = T;
}

impl Wrapper for Rc {
    type Type<T> = std::rc::Rc<T>;
}

impl Wrapper for Arc {
    type Type<T> = std::sync::Arc<T>;
}

pub trait List {
    type Type<T>;
}

pub struct Vec;

impl List for Vec {
    type Type<T> = std::vec::Vec<T>;
}
